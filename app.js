var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multipart = require('connect-multiparty');
var marks = require('./routes/marks');
var exam = require('./routes/exam');
var router = express.Router();
//var users = require('./routes/users');

var app = express();

//console.log(examController);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//app.set('trust proxy', 1) // trust first proxy

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/marks',express.static(path.join(__dirname, 'public')));
app.use(multipart());
app.use('/marks', marks);
app.use('/marks/exam', exam);
//app.use('/users', users);

//The 404 Route (ALWAYS Keep this as the last route)
app.use('/marks',router.get('*', function(req, res){res.redirect('/marks');}));

// catch 404 and forward to error handler
/*app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
*/
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
