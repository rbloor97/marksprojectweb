var jwt = require('jsonwebtoken');
var status = require('http-status');
var fs = require('fs');
var mkdirp = require('mkdirp');
var secret = process.env.SECRET_TOKEN_ENCODED || 'EvaluationHelperSecret';

module.exports = {
  readExam:readExam,
  uploadExam:uploadExam
};

function readExam(token,idCourse,idAssignment,idStudent,callback){

  var path = "pdf/exams/"+idCourse+"/"+idAssignment+"/"+idStudent+".pdf";

  // Valida que no exista problemas al decodificar el token
  try {
    var decoded = jwt.verify(token, secret);
  } catch(err) {
    callback(true,403,err);
  }

  // Valida que el token pertenezca al estudiante dueño del examen
  if(decoded.idStudent!=idStudent) {
    callback(true,403,"El token de acceso no coincide con el el id del estudiante");
  }

  var readStream = fs.createReadStream(path);

  readStream.on('open', function () {
    var token = jwt.sign({ idStudent: idStudent }, secret);
    console.log(token);
    callback(false,200,readStream);

  });

  // This catches any errors that happen while creating the readable stream (usually invalid names)
  readStream.on('error', function(err) {
    callback(true,status.NOT_FOUND,"Error al leer el PDF");
  });
}

function uploadExam(file,idCourse,idAssignment,idStudent,callback){

  if(file.type != 'application/pdf'){
    console.log("Error: EL archivo no está en formato PDF.");
    callback(status,"Error, el archivo a subir debe de estar en formato pdf" );
  }


  // Path temporal del archivo enviado
  var tmp_path = file.path;
  // Path en donde se guardará el examen
  var path_exam = "pdf/exams/"+idCourse+"/"+idAssignment;

  checkDirectory(path_exam, function() {

    var is = fs.createReadStream(tmp_path);
    var os = fs.createWriteStream(path_exam+"/"+idStudent+".pdf");

    is.pipe(os);

    // Archivo temporal eliminado
    is.on('end',function(){fs.unlinkSync(tmp_path)});

    os.on('close',function(){
      // Genera un token que contien la matricula del estudiante, para poder acceder al examen
      var token = jwt.sign({ idStudent: idStudent }, secret);
      console.log("PDF guardado: "+path_exam+"/"+idStudent+".pdf");
      callback(status.OK,"/marks/exam/"+idCourse+"/"+idAssignment+"/"+idStudent+"?token="+token );
    });

  });
}

/**
 * Verifica que un directorio (junto con sus subdirectorios) exista, caso
 * contrario lo crea.
 * @method checkDirectory
 * @param {string} directory Directorio a comprobar que exista
 * @param {function} callback Funcion a ejecutarse despues de comprobar la
 * existencia del directorio
 * @return
 */

function checkDirectory(directory,callback){
    fs.stat(directory,function(err,stats){

      // Si el directorio no existe, crea el directorio con sus subdirectorios
      if(err){

        mkdirp(directory, function (err) {
          if (err) console.error(err)
          else {
            console.log("Directorio creado: "+directory);
            callback();
          }
        });
      }
      else{
        callback();
      }
    });
}
