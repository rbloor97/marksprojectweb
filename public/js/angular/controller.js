'use strict'

marksApp.controller('IndexController',function($scope,$http,$sce){
  $scope.show_pdf = false;
  $scope.show_loader = false;
  $scope.content = null;
  // Funcion que se encarga de enviar el examen al server
  $scope.sendFile = function(){

    if($scope.exam_pdf== null){
      Materialize.toast("Error, Seleccione un examen", 3000,'toast-danger');
      return;
    }
    // Se adjunta el examen al formulario
    var formData = new FormData();
    formData.append('archivo',$scope.exam_pdf);

    $scope.show_loader = true;
    // Realiza la peticion al server
    $http.post('add_marks', formData, {
       transformRequest: angular.identity,
       headers: {'Content-Type': undefined},
       responseType: 'arraybuffer'//se devuelve un arrayBuffer que contiene el pdf
    })

    .then(function successCallback(response) {

      var file = new Blob([(response.data)], {type: 'application/pdf'});
      var fileURL = URL.createObjectURL(file);
      $scope.content = $sce.trustAsResourceUrl(fileURL);
      $scope.show_pdf = true;
      $scope.show_loader = false;

      Materialize.toast('PDF Generado correctamente', 3000,'toast-succesful');

    }, function errorCallback(response) {

      var decodedString = String.fromCharCode.apply(null, new Uint8Array(response.data));
      var obj = JSON.parse(decodedString);

      $scope.show_loader = false;

      Materialize.toast(obj.message, 3000,'toast-danger');
      console.log(c);
    });

  };
}
);
