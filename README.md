# Descripción General

Este sistema web está completamente hecho en NodeJs con sus dependencias especificadas en su archivo de configuración correspondiente.

Añade las marcas específicas para cada página del examen así como el campo cédula para el respectivo reconocimiento, de está manera deja a conveniencia del profesor en que software de creación de documentos realiza su examen, esto debido a que el sistema recibe como entrada un documento en PDF.

Adicionalmente el profesor deberá tener en cuenta algunas consideraciones necesarias antes de subir su examen al sistema:

Es responsabilidad del profesor dejar los espacios necesarios para la colocación de las marcas y del campo cédula al momento de crear su examen. Para esto el sistema provee una plantilla de Word básica, la cual el profesor puede usarla si así lo desea.


1. El sistema solo acepta 2 tipos de formatos: A4 y Carta.
2. El sistema solo agrega las marcas a las 10 primeras páginas del documento, por lo cual el profesor debe de procurar que su examen no exceda dicho número de páginas.
3. Asimismo el sistema provee 2 funcionalidades adicionales, las cuales permiten que el sistema ReconocimientoAutoEval pueda subir los exámenes escaneados y que el sistema de calificación de Sidweb pueda consultarlos.

Al momento de subir un examen al sistema, este genera un token de acceso el cual es enviado como respuesta del requerimiento.

    /exam/idCurso/idTarea/idEstudiante?token=Token_codificado_con_id_estudiante
    
Este token tiene codificado el id del estudiante dueño del examen, permitiendo consultar el examen solo a la persona que disponga de dicho token (Estudiante dueño del examen y Profesor) ambos pudiéndolo consultar solo desde el sistema de calificación de Sidweb.

# Despliegue

Para poder instalar las dependencias correr el siguiente comando:
    
    npm install

Para ponerlo en ejecución:
    
    npm start
