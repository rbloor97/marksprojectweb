var express = require('express');
var hummus = require('hummus');
var router = express.Router();
var fs = require('fs');
var status = require('http-status');
var jwt = require('jsonwebtoken');

/* GET home page. */
/*router.get('/', function(req, res, next) {
   res.send('/public/index.html');
});
*/

router.post('/add_marks',function(req,res){
  console.log("Peticion para agregar marcas");
  // PDF enviado como parametro
  var archivo = req.files.archivo;
  //console.log(archivo);
  if(archivo.type != 'application/pdf'){
    console.log("Error: EL archivo no está en formato PDF.");
    res.status(status.BAD_REQUEST)
    .json({ message: "Error, el archivo a subir debe de estar en formato pdf" });
  }

  else{
   // Path temporal del archivo enviado
   var path = archivo.path;
   // Nombre del archivo que temporalmente sera guardado en la raiz del proyecto
   var newPath = "pdf/tmp/"+Math.random().toString(36).substring(7)+".pdf";
   console.log("new Path "+newPath);
   var is = fs.createReadStream(path);
   var os = fs.createWriteStream(newPath);

   is.pipe(os);

   //El archivo temporal es eliminado
   is.on('end', function() {fs.unlinkSync(path)});

   os.on('close', function() {
     try {
       agregarMarcas(newPath,res);
     } catch (e) {
       // Puede ser que el PDF no pueda ser leido
       console.log("Error al parsear el PDF",e);
       res.status(status.BAD_REQUEST)
       .json({ message: "Error al parsear el PDF" });
     } finally {
       // Se elimina el archivo
       fs.unlinkSync(newPath);
       console.log("Archivo "+newPath+" eliminados");
     }
   });
  }
});


/*
 * Verifica que las dimensiones del pdf sean tamaño carta o A4
 */
function check_dimension(pdf_dimensions){

  // Dimensiones
  var pdf_pixels_dimensions = {letter:[612,792],
                               A4:[595,842]};

  var pdf_dimensions = [Math.round(pdf_dimensions[2]),Math.round(pdf_dimensions[3])];

  if(pdf_dimensions[0] == pdf_pixels_dimensions.A4[0] && pdf_dimensions[1] == pdf_pixels_dimensions.A4[1]){
    console.log("Dimension del PDF: A4");
     return "A4";
  }
  else if (pdf_dimensions[0] == pdf_pixels_dimensions.letter[0] && pdf_dimensions[1] == pdf_pixels_dimensions.letter[1]) {
    console.log("Dimension del PDF: CARTA");
    return "LETTER";
  }
  return null;

}

function agregarMarcas(path,res){
  // Direccion del pdf original
  var dir_pdf = path;
  // Direccion del pdf modificado
  var dir_pdf_mod = dir_pdf;
  //Directorio de las marcas
  var dir_marks = 'marks/img_marks_app';
  //var dir_marks = 'marks/img_marks_2';
  //Directorio de la imagen cedula
  var cedula_img = 'marks/cedula.jpg';
  // Posicion de las marcas (0 es la posicion del campo cedula)
  //var marks_position = {0:[155,730],1:[42,730],2:[483,730],3:[40,48],4:[483,48]};
  //var marks_position = {0:[170,680],1:[42,676],2:[500,676],3:[42,48],4:[483,48]};
  /*var marks_position = {"A4":{0:[155,730],1:[0,775],2:[525,775],3:[0,0],4:[525,0]} ,
                        "LETTER":{0:[170,680],1:[42,676],2:[500,676],3:[42,48],4:[483,48]}};*/
/*  var marks_position = {"A4":{0:[155,730],1:[42,730],2:[483,730],3:[40,48],4:[483,48]} ,
                        "LETTER":{0:[170,680],1:[42,676],2:[500,676],3:[42,48],4:[483,48]}};*/
  var marks_position = {"A4":{0:[176,720],1:[75,715],2:[460,715],3:[75,110],4:[460,110]} ,
                          "LETTER":{0:[170,680],1:[45,676],2:[500,676],3:[45,48],4:[483,48]}};

  // El número maximo de paginas a las que se puede agregar las marcas
  var max_pages = 10;

    var pdfReader = hummus.createReader(dir_pdf);

    // Numero de paginas del pdf
    var pages = pdfReader.getPagesCount();

    var pdfWriter = hummus.createWriterToModify(dir_pdf, {modifiedFilePath: dir_pdf_mod});

    // Dimensiones de la pagina pdf
    var pdf_dimensions = check_dimension(pdfReader.parsePage(0).getMediaBox());

    if(pdf_dimensions == null){
      console.log("Error la dimension del PDF No esta soportado ",pdfReader.parsePage(0).getMediaBox());
      res.status(status.BAD_REQUEST)
      .json({ message: "Error, el PDF debe de estar en formato A4 o Carta" });
      return
    }
    marks_position = marks_position[pdf_dimensions];
    // Recorre las paginas del pdf
    for(var page = 0 ; page < ((pages<max_pages) ? pages:max_pages) ; page++){

      var pageModifier = new hummus.PDFPageModifier(pdfWriter,page,true);
      pageModifier.startContext().getContext().drawImage(marks_position[0][0],marks_position[0][1],cedula_img,{transformation:{width:250,height:80}})
      //pageModifier.startContext().getContext().drawImage(marks_position[0][0],marks_position[0][1],cedula_img,{transformation:{width:300,height:100}})

      for(var position = 1 ;position <= 4 ; position++){
        var dynamic_dir_marks = dir_marks + '/'+(page+1)+'/'+position+'.jpg';
          //var dynamic_dir_marks = dir_marks + '/'+(page+1)+'/'+position+'.png';
        pageModifier.startContext().getContext().drawImage(marks_position[position][0],marks_position[position][1],dynamic_dir_marks,{transformation:{width:63,height:63}})
        //pageModifier.startContext().getContext().drawImage(marks_position[position][0],marks_position[position][1],dynamic_dir_marks,{transformation:{width:70,height:70}})
      }

      pageModifier.endContext().writePage();
    }
    pdfWriter.end();

    var file = fs.createReadStream(dir_pdf_mod);
    //res.sendFile(dir_pdf_mod,{ root: __dirname });
    res.writeHead(200, {'Content-Type': 'application/pdf'});
    file.pipe(res);
    console.log("Las marcas se agregaron correctamente");
}

module.exports = router;
