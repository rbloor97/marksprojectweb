var express = require('express');
var router = express.Router();
var status = require('http-status');

var examController = require('../controllers/exam');

/**
 * Get Secret to encoded the token
 */

router.get('/:idCourse/:idAssignment/:idStudent',function(req,res,next){

  // Verifica que exista el token para la validacion
  if(!req.query.token) {
    return res
      .status(403)
      .json({message: "Tu petición no contiene un token de acceso"});
  }
  examController.readExam(req.query.token,req.params.idCourse,req.params.idAssignment,req.params.idStudent,(error,statusCode,data)=>{
    if(error){
      var message = data;
      res.status(statusCode).json({message: message});
    }
    else{
      var readStream = data;
      res.writeHead(200, {'Content-Type': 'application/pdf'});
      readStream.pipe(res);
    }
  });
});

router.post('/course/:idCourse/assignment/:idAssignment/student/:idStudent/upload',function(req, res ,next){

  // Verificar token de acceso
  console.log("subir archivo");
  var file = req.files.archivo;

  examController.uploadExam(file,req.params.idCourse,req.params.idAssignment,req.params.idStudent,(status,data)=>{
    res.status(status)
    .json({ message:data});
  });
});



module.exports = router;
